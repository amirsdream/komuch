import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {FactorPage} from '../factor/factor';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  customer_id:any;
  token:any;
  stage1:any='تعداد نان';
  stage2:any='جمع فاکتور';
  stage3:any='زمان تحویل';
  // stage4:any;
  active:any=[false,false,false];
  loginparameter = "token=";
  factor = FactorPage;
  n:any;



  constructor( private storage: Storage,public navCtrl: NavController, public navParams: NavParams) {
    // If we navigated to this page, we will have an item available as a nav param



        this.storage.forEach((value, key) => {
          if (key == 'total') {
            this.stage1 = value;
            if (this.stage1!=0){
              this.active[0]=true;
            }
            else{
              this.stage1='تعداد نان';
            }
            // console.log(customer_id);
          } if (key == 'finalprice') {
            this.stage2 = value;
            if (this.stage2!=0 && this.stage2!=2000){
              console.log( this.stage2!=2000)
              this.active[1]=true;
            }
            else{
              this.stage2='جمع فاکتور';
            }
          }if (key =='date_text') {
            this.stage3 = value;
            // console.log(JSON.parse(this.stage2));
            if (this.stage3!=0){
              this.active[2]=true;
            }
            else{
              this.stage3='زمان تحویل';
            }
          }
          if (key == 'customer_id') {
            this.customer_id = value;
            // console.log(customer_id);
          } if (key == 'token') {
            this.token = value;
          }
          // if (key == 'stage4') {
          //   this.stage2 = value;
          // }
        })

  }

  getMyStyles(x) {
    // console.log(x)
    let myStyles = {
      'color': x ?  'orange' : 'black',
      'border-left': '1px solid ',
      'height': '60px',
    };
    return myStyles;
  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.loginparameter + this.token + "&customer_id=" + this.customer_id + "&"});
  }

  ionViewDidEnter(){
    this.active=[false,false,false];
    this.storage.forEach((value, key) => {
      if (key == 'total') {
        this.stage1 = value;
        this.n =value;
        if (this.stage1!=0){
          this.active[0]=true;
        }
        else{
          this.stage1='تعداد نان';
        }
        // console.log(customer_id);
      } if (key == 'finalprice') {
        this.stage2 = value;
        if (this.stage2!=0 && this.stage2!=2000){
          this.active[1]=true;
        }
        else{
          this.stage2='جمع فاکتور';
        }
      }if (key =='date_text') {
        this.stage3 = value;
        // console.log(JSON.parse(this.stage2));
        if (this.stage3!=0 ){
          this.active[2]=true;
        }
        else{
          this.stage3='زمان تحویل';
        }
      }
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      } if (key == 'token') {
        this.token = value;
      }
      // if (key == 'stage4') {
      //   this.stage2 = value;
      // }
    })
  }

}
