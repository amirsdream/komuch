import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LoaderProvider } from '../pages/login2/login2.loader';
import { Storage } from '@ionic/storage';
import {baseUrl} from "../base_url";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

// const params = new HttpParams()
//   .set('txtmobile', '09309129143')
//   .set('flag', '0')
//   .set('token','1');

// let params = new HttpParams();





var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class BasketService {

  constructor(private http:HttpClient,private loader: LoaderProvider,private storage: Storage) {}

  // Uses http.get() to load data from a single API endpoint





  getbasket(food) {

      let x: any;
      console.log(food);
      let body = JSON.stringify(food);
      return this.http.post(baseUrl+'front/userOrder/getBasketPrime',
        food, httpOptions)
        .map(res => res)


  }
  getTimeBox() {

    return this.http.post(baseUrl+'front/pages/getTimeBox',
      {},httpOptions)
      .map(res => res)


  }

}
