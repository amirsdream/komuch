import {Component} from '@angular/core';
import {NavController, ViewController, NavParams} from 'ionic-angular';
import {BreadPage} from '../bread/bread';
import {LoaderProvider} from '../login2/login2.loader';
import {ProfileService} from './profile.service';
import {Storage} from '@ionic/storage';
// import {MapPage} from'../gmaps/gmaps';
import {FactorPage} from '../factor/factor';
// import {NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult} from '@ionic-native/native-geocoder';
import {PayMetPage} from '../paymet/paymet';
// import {LatLng} from "@ionic-native/google-maps";

@Component({
  selector: 'page-home',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  gcode = "خودتو تو نقشه پیدا کن!";
  customer_id = "";
  token = "";
  first_name = " ";
  last_name = " ";
  // map = MapPage;
  param1: any;
  param2: any;
  param3: any;
  param5: any;
  param4: any;
  factor=FactorPage;
  n;
  lat: any="خالی";
  lng: any="خالی";
  addr: any;
  newaddr: any = [];
  // private location: LatLng;
  keys: any = [];
  input;
  last;
  paymet = PayMetPage;
  msg_param:any;
  msg_time:any;

  constructor( public navParams: NavParams, private storage: Storage, public AddrService: ProfileService, private loader: LoaderProvider, public navCtrl: NavController, private viewCtrl: ViewController) {
    this.param2 = navParams.get('param2');
    this.param1 = navParams.get('param1');
    this.param3 = navParams.get('param3');
    this.msg_param = navParams.get('msg_param');
    this.msg_time = navParams.get('msg_param');

    console.log(this.param3);


    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      }
      if (key == 'token') {
        this.token = value;
      } //store these values as you wish

      if (key == 'first_name') {
        if (value!=null) {
          this.first_name = value;
        }
      } //store these values as you wish

      if (key == 'last_name') {
        if (value!=null) {
          this.last_name = value;
        }
      } //store these values as you wish

      if (key == 'addresses') {
        this.addr = JSON.parse(value);
        console.log(value)
      }
    }).then(resp => this.getData())


  }

  getData() {
    for (let key in this.addr) {
      console.log(key);
      this.keys.push({key: this.addr[key]['address_text'], value: this.addr[key]['id']});
      console.log(this.keys);
    }
    return this.addr
  }

  // goToMap() {
  //   this.navCtrl.push(this.map, {param1: this.param1, param2: this.param2, param3: this.param3});
  // }

  select(item) {
    this.param4 = item;
    console.log(this.param4)
  }



  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.param3});
  }

  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
      if (key=='total'){
        this.n =value;
      }
    })
  }

}
