import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LoaderProvider } from './login2.loader';
import { Storage } from '@ionic/storage';
import {baseUrl} from "../../base_url";
import { ToastController } from 'ionic-angular';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

const params = new HttpParams()
  .set('txtmobile', '09309129143')
  .set('flag', '0')
  .set('token','1');

// let params = new HttpParams();

var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class LoginService2 {

  constructor(private toastCtrl: ToastController,private http:HttpClient,private loader: LoaderProvider,private storage: Storage) {}

  // Uses http.get() to load data from a single API endpoint



  createLogin(food,response) {


    let x: any;
    console.log(food);
    let body = JSON.stringify(food);
    console.log(params);

    this.http.post(baseUrl+'front/useraccounts/login',
      food, httpOptions)
      .subscribe(res => {
        // this.loader.hide();
        x = JSON.parse(JSON.stringify(res));
        console.log(x.type);
        if (x.type=="success"){
          this.storage.set('customer_id',x.data.customer_id);
          this.storage.set('token',x.data.token);
          this.storage.set('loggedin',x.type);
          this.storage.set('first_name',x.data.first_name);
          this.storage.set('last_name',x.data.last_name);
          // this.http.post(baseUrl+'front/page/popup',
          //   food, httpOptions)
          //   .subscribe( res => {
          //
          //   });
          response(res)}else{
          let toast = this.toastCtrl.create({
            message: x.msg,
            duration: 3000,
            position: 'top'
          });
            toast.present();
          }
          response(false);


        console.log(JSON.stringify(res));
      }, error => {
        this.loader.hide();
        console.log(JSON.stringify(error));
      });

  }

  getPopupLogin() {

    return this.http.get(baseUrl+'front/pages/popup')
      .map(res => res)
  }
}
