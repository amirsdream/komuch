import {Component} from '@angular/core';
import {NavController, MenuController } from 'ionic-angular';
import {Network} from'@ionic-native/network';
import {LoginService} from './login.service';
import {LoginPage2} from '../login2/login2';
import {NativeStorage} from '@ionic-native/native-storage';
import {LoaderProvider} from '../login2/login2.loader';
import {baseUrl} from "../../base_url";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

let loginparameter: string;
@Component({
  selector: 'page-home',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginpage2 = LoginPage2;
  loginparameter = "token=1&flag=0&txtmobile=";
  logintel = "09";
  loginvalue = {'Posting params': this.loginparameter};
  form : FormGroup;
  validation_messages = {
    'myField': [
      { type: 'pattern', message: '' }
    ]
  }

  constructor(public formBuilder: FormBuilder,private menu: MenuController,private loader: LoaderProvider, private nativeStorage: NativeStorage, public navCtrl: NavController, private network: Network, public LoginService: LoginService) {
    console.log("------------------")
    this.form = this.formBuilder.group({
      myField: ['', Validators.pattern('[0-9]{11}')]
    });

    // Initial value for the field.
    this.form.get('myField').setValue('09');

  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  logForm() {
    this.logintel=this.form.get('myField').value;
    this.loader.createLoader();
    this.loader.show();
    console.log(this.logintel);
    this.LoginService.createLogin(this.loginparameter + this.logintel + "&", response => this.navCtrl.push(this.loginpage2, {
      logintel: this.logintel,
    }));
    // this.nativeStorage.setItem('myitem', {tel: this.logintel})
    //   .then(
    //   () => console.log('Stored item!'),
    //   error => console.error('Error storing item', error)
    // );

  }

}
