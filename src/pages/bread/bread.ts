import {Component, Pipe, PipeTransform} from '@angular/core';
import {NavController, NavParams, ViewController,} from 'ionic-angular';
import {data1, products, BreadService} from './bread.service';
import {LoaderProvider} from '../login2/login2.loader';
import {FactorPage} from '../factor/factor';
import {Storage} from '@ionic/storage';
import {Observable} from 'rxjs/Observable';


// @Pipe({name: 'keys'})
// export class KeysPipe implements PipeTransform {
//   transform(value, args:string[]) : any {
//     let keys = [];
//     for (let key in value) {
//       keys.push({key: key, value: value[key]});
//     }
//     return keys;
//   }
// }

@Component({
  selector: 'page-home',
  templateUrl: 'bread.html',
})


export class BreadPage {
  private currentNumber = 0;
  helper2:any;
  categories: any;
  isproducts: data1;
  param3: any;
  param4: any;
  helper: any = [];
  products: products;
  response: string;
  param2: string;
  x: any;
  keys: any = [];
  // newkeys : any=[];
  newkeys: Array<any> = [];
  y = 0;
  buttonDisabled: boolean = true;
  factor = FactorPage;
  params: any;
  n: any;
  cat_name: any;


  constructor(private storage: Storage, public viewctrl: ViewController, public loader: LoaderProvider, public navCtrl: NavController, public navParams: NavParams, public bread: BreadService) {
    this.keys = navParams.get('param1');
    this.param2 = navParams.get('param2');
    this.params = this.param2;
    this.storage.forEach((value, key) => {
      if (key == 'cat_name') {
        this.cat_name = value;
        // console.log(customer_id);
      }
    })

  }

  //

  ionViewDidLoad() {



    // this.loader.createLoader();
    // this.loader.show();
    // // console.log(this.param2);
    // this.x = this.bread.getbread(this.categories, this.param2).
    //   subscribe(res => {
    //
    //   // console.log(JSON.stringify(res));
    //
    //   this.isproducts = res as data1;
    //   // console.log(JSON.stringify(this.isproducts));
    //   this.products = this.isproducts.data;
    //
    //   for (let key in this.products) {
    //     // keys.push({key: key, value: value[key]});
    //     for(let x in this.products[key]){
    //       this.products[key][x]['num']=0;
    //       this.keys.push({key:this.products[key][x],value:this.y});
    //       // console.log(value.key['id']);
    //
    //     }
    //   // console.log(this.products);
    //   };
    //   this.loader.hide();
    // });


    console.log(this.x);


  }


  private increment(entry) {
    this.keys[entry].value++;
    // console.log(this.keys[entry].value);
    this.buttonDisabled = false;

  }

  private decrement(entry) {

    if (this.keys[entry].value < 1) {
      this.buttonDisabled = true;
      return 0;
    }
    this.currentNumber--;
    this.keys[entry].value--;
  }

  ionViewDidEnter() {
    this.storage.forEach((value, key) => {
      if (key == 'total') {
        this.n = value;
      }
    })
  }

  async newkey(): Promise<any> {
    this.helper = []
    for (let key in this.keys) {
      this.param3 = "";
      // console.log(this.keys[key].value)
      if (this.keys[key].value > 0) {
        this.newkeys.push(this.keys[key]);
        this.param3 = this.param2 + "&product_id=" + this.keys[key].key.id + "&qty=" + this.keys[key].value + "&vaherprice=" + this.keys[key].key.price;
        this.helper.push( this.bread.book(this.param3,  response => 0));
        console.log(this.helper)
      }
    }

    return Promise.all(this.helper);
  }

  private book() {
    this.loader.createLoader();
    this.loader.show();

    this.newkey().then(response =>
        this.pushNext()
        // this.loader.hide(),
        // this.navCtrl.push(this.factor, {param1: this.param4, param2: this.params,})
      // this.bread.book(this.param2,response =>
      // this.navCtrl.push(this.factor, {param1: this.param4, param2: this.params,})
    )


  }

  pushNext(){
    this.loader.hide();
    this.navCtrl.push(this.factor, {param1: this.param4, param2: this.params,});
  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.param2 + "&"});
  }

  ionViewDidLeave() {


  }


}
