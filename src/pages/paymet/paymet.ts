import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';
import {PayMetService} from './paymet.service';
import {LoaderProvider} from '../login2/login2.loader';
import {Storage} from '@ionic/storage';
import {PardakhtPage} from '../pardakht/pardakht';
import {WalletPage} from '../wallet/wallet';
import {FactorPage} from '../factor/factor';
import {DiscountPage} from '../discount/discount';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'paymet.html',
})
export class PayMetPage {

  param3: any;
  param2: any;
  param1: any;
  param4: any;
  param5: any;
  param6: any;

  input: any;
  first_name: any;
  last_name: any;
  msg_param: any;
  lat: any = "خالی";
  lng: any = "خالی";
  finalprice: string = "";
  newone: string = "";
  pardakht = PardakhtPage;
  wallet = WalletPage;
  x: any;
  n: any;
  isVisible = 'visible';
  isHidden = 'hidden';
  factor = FactorPage;
  discount_page = DiscountPage;
  used_discount = false;
  sel: any = 0;
  credi: any = 0;


  constructor(private toastCtrl: ToastController, private storage: Storage, private loader: LoaderProvider, private pay: PayMetService, private alertCtrl: AlertController, private platform: Platform, public navParams: NavParams, public navCtrl: NavController, private viewCtrl: ViewController) {
    this.param3 = navParams.get('param3');
    this.param2 = navParams.get('param2');
    this.param4 = navParams.get('param4');
    this.param1 = navParams.get('param1');
    this.param5 = navParams.get('param5');
    this.param6 = navParams.get('param6');
    this.msg_param = navParams.get('msg_param');
    console.log(this.msg_param)
    console.log(this.param6)

    this.lat = this.param5[0];
    this.lng = this.param5[1];
    console.log(this.param5)
    this.finalprice = this.param2['order_info']['final_price']
    console.log(this.finalprice)
    this.storage.forEach((value, key) => {

      if (key == 'first_name') {
        this.first_name = value;
      } //store these values as you wish

      if (key == 'last_name') {
        this.last_name = value;
      } //store these values as you wish
      if (key == 'credit') {
        this.credi = value;
      } //store these values as you wish

    })
    if (this.param6 != undefined) {
      this.finalprice = this.param6.data.orders[0].final_price;
      this.used_discount = true;
      console.log(this.finalprice)

    }
  }

  getDiscount() {
    let x: any;
    this.loader.createLoader();
    this.loader.show();
    this.pay.getdiscount(this.param3 + "&txtoff=" + this.input + "&").subscribe(res => {
      this.loader.hide();
      console.log(JSON.stringify(res));
      x = JSON.parse(JSON.stringify(res));
      if (x.type == 'error') {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: x.msg,
          buttons: ['متوجه شدم']
        });
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: 'تخفیف با موفقیت اعمال شد',
          buttons: ['متوجه شدم']
        });
        alert.present();
      }
    }, error => {
      this.loader.hide();
      console.log(JSON.stringify(error));
    });
  }

  getwallet() {
    this.navCtrl.push(this.wallet, {param2: this.param3});
  }

  gToPay() {
    if (this.sel) {
      let alert = this.alertCtrl.create({
        title: 'توجه',
        message: 'آیا از پرداخت مطمئنید؟',

        buttons: [
          {
            text: 'انصراف',
            role: 'cancel',
            handler: () => {

            }
          },
          {
            text: 'تایید',
            handler: (data) => {
              this.goToPay();

            }


          }
        ]
      });
      alert.present();
    }
    else {
      let toast = this.toastCtrl.create({
        message: 'نحوه پرداختو انتخاب کنید',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  goToPay() {
    let x: any;
    this.loader.createLoader();
    this.loader.show();
    if (this.param4.value == "new") {
      this.newone = "newaddress=" + this.param4.key + "&";
    }
    console.log(this.param1);
    this.pay.goTo(this.param3 + "&txtfname=" + this.first_name + "&" + "txtlname=" + this.last_name + "&"
      + "txtaddress_id=" + this.param4.value + "&"
      + this.newone
      + "ship_method=" + "peyk" + "&"
      + "lng=" + this.lng + "&"
      + "lat=" + this.lat + "&"
      + "agree=" + "1" + "&"
      + "payment_method=" + this.sel + "&" + this.param1[0] + "=" + this.param1[1] + "&").subscribe(res => {
      this.loader.hide();
      console.log(res);
      // console.log(res);
      this.x = JSON.parse(JSON.stringify(res));
      console.log(this.x);
      if (this.x.type == 'error') {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.x.msg,
          buttons: ['متوجه شدم']
        });
        alert.present();
      }
      else {
        if (this.sel == 'credit' || this.sel == 'inplace') {
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: this.x.msg,
            buttons: ['متوجه شدم']
          });
          alert.present().then(res => {
            this.storage.set('finalprice', 0);
            this.storage.set('timeinfo', 0);
            this.storage.set('total', 0);
            this.navCtrl.popToRoot();
          });
        }
      }
      if (this.sel == 'saman') {
        this.navCtrl.push(this.pardakht, {
          token: this.x.Token,
          url: this.x.url,
          type: this.x.type,
          redirect: this.x.Redirect
        });
      }
      // if (this.sel == 'inplace') {
      //   this.navCtrl.push(this.pardakht, {
      //     token: this.x.Token,
      //     url: this.x.url,
      //     type: this.x.type,
      //     redirect: this.x.Redirect
      //   });
      // }
    }, error => {
      this.loader.hide();
      console.log(JSON.stringify(error));
    });
  }

  setDiscount(){
    if(!this.used_discount) {
      this.navCtrl.push(this.discount_page, {
        param1: this.param1,
        param2: this.param2,
        param3: this.param3,
        param4: this.param4,
        param5: this.param5,
        msg_param:this.msg_param ,
      });
    }
  }
  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.param3});
  }



  ionViewDidEnter() {
    this.storage.forEach((value, key) => {
      if (key == 'total') {
        this.n = value;
      }
    })
  }

}
