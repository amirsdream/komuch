import {Component} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular';
import {LoaderProvider} from '../login2/login2.loader';
import {FactorService} from './factor.service';
import {TimePage} from '../time/time';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'factor.html'
})
export class FactorPage {

  param2: string;
  x: any;
  info: any;
  keys: any = [];
  time = TimePage;
  result: any = [];
  factors:any;
  total: number = 0;
  finalprice:number = 0;

  constructor(private toastCtrl: ToastController,private storage: Storage,private alertCtrl: AlertController, public navParams: NavParams, public factor: FactorService, private loader: LoaderProvider, public navCtrl: NavController) {
    this.param2 = navParams.get('param2');
    this.storage.set('total',this.total);
  }

  ionViewDidLoad() {
    this.loader.createLoader();
    this.loader.show();

    this.x = this.factor.getfactor(this.param2).subscribe(res => {
      this.loader.hide();
      console.log(JSON.stringify(res));
      this.info = res;
      // this.finalprice = Number(this.info['order_info'][0]['final_price'])
      // this.storage.set('finalprice',this.finalprice);
      for (let key in this.info['products']) {
        this.keys.push({key: this.info['products'][key]});
        this.total = this.total + Number(this.info['products'][key]['quantity']);
        this.storage.set('total',this.total);

        console.log(this.total);
      }


      // for (let key in this.info['order_info']){
      this.result.push({key: this.info['order_info']});
      this.storage.set('finalprice',this.info['order_info']['final_price'])
      // console.log(this.result);
      // }

    }, error => {
      this.loader.hide();
      console.log(JSON.stringify(error));
    });

  }

  presentConfirm(id) {
    let alert = this.alertCtrl.create({
      title: 'حذف',
      message: 'آیا از حذف مطمنید؟',
      buttons: [
        {
          text: 'خیر',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'بله',
          handler: () => {
            this.deleteid(id);
          }
        }
      ]
    });
    alert.present();
  }

  deleteid(id) {
    this.loader.createLoader();
    this.loader.show();
    this.factor.deletefactor(this.param2 + "&id=" + id + "&")
      .subscribe(res => {
        this.x = this.factor.getfactor(this.param2).subscribe(res => {
          this.loader.hide();
          console.log(JSON.stringify(res));
          this.keys = [];
          this.info = res;
          this.total = 0;
          // this.finalprice = Number(this.info['order_info'][0]['final_price'])
          // this.storage.set('finalprice',this.finalprice);
          if (this.info['products']==null){
            this.storage.set('total',0);
          }
          for (let key in this.info['products']) {
            console.log(key);


            this.keys.push({key: this.info['products'][key]});
            this.total = this.total + Number(this.info['products'][key]['quantity']);

            this.storage.set('total',this.total);
            console.log(this.keys);
          }
          this.result = [];
          this.result.push({key: this.info['order_info']});
          this.storage.set('finalprice',this.info['order_info']['final_price']);
          console.log(this.info['order_info']['final_price']);
          if (this.info['order_info']['final_price']==2000){
            this.storage.set('total',this.total);
            this.storage.set('finalprice',0);
            this.storage.set('date_text',0);
          }
        }, error => {
          this.loader.hide();
          console.log(JSON.stringify(error));
        });
      });
  }

  next() {
    if (this.total > 0) {

      this.navCtrl.push(this.time, {
        param2: this.info, param1: this.param2
      });
    }else {
      let toast = this.toastCtrl.create({
        message: 'سفارشی ندارید',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    }


}
