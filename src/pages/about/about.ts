import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FactorPage} from '../factor/factor';
import {Storage} from '@ionic/storage';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  customer_id:any;
  token:any;
  orders:any = [];
  factor = FactorPage;
  loginparameter = "token=";
  n:any;

  constructor(public navCtrl: NavController,private storage: Storage, public navParams: NavParams) {
    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      } if (key == 'token') {
        this.token = value;
      }
    })
  }
  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
      if (key=='total'){
        this.n =value;
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.loginparameter + this.token + "&customer_id=" + this.customer_id + "&"});
  }

}
