import {Component} from '@angular/core';
import {NavController, ViewController} from 'ionic-angular';
import {BreadPage} from '../bread/bread';
import {LoaderProvider} from '../login2/login2.loader';
import {HomeService2} from './home.service';
import {Storage} from '@ionic/storage';
// import {MapPage} from'../gmaps/gmaps';
import {FactorPage} from '../factor/factor';
import {LoginPage} from "../login/login";
import {BasketService} from "../../app/app.service";
import {TimeService} from "../time/time.service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  customer_id = "";
  token = "";
  bread = BreadPage;
  // map = MapPage;
  factor = FactorPage;
  loginparameter = "token=";
  pushPage: any;
  n:any;
  x:any;
  cart_order = 0;
  constructor(private storage: Storage,private basket: BasketService,private time: TimeService, public HomeService2: HomeService2, private loader: LoaderProvider, public navCtrl: NavController, private viewCtrl: ViewController) {
    this.pushPage = BreadPage;

    // this.storage.forEach((value, key) => {
    //   if (key == 'customer_id') {
    //     this.customer_id = value;
    //     // console.log(customer_id);
    //   }
    //   if (key == 'token') {
    //     this.token = value;
    //   } //store these values as you wish
    //   if (key=='total'){
    //     this.n =value;
    //   }
    // });


    // this.storage.get('loggedin').then((val) => {
    //   if (val == "success") {
        // this.rootPage = HomePage;
        console.log(1);
        this.storage.forEach((value, key) => {
          if (key == 'customer_id') {
            this.customer_id = value;
            // console.log(customer_id);
          } if (key == 'token') {
            this.token = value;
          } })
          .then(resp =>
            this.basket.getbasket("token="+ this.token + "&customer_id=" + this.customer_id + "&")
            .subscribe(res => {
              console.log(JSON.stringify(res));
              let orders = JSON.parse(JSON.stringify(res));
              orders.data.orders.forEach(order=>{
                order.products.forEach(pr=>{
                  this.cart_order += parseInt(pr.quantity);
                })
              });
              this.storage.set("total",this.cart_order);
              // console.log(this.cart_order);

              this.time.getprofile("token="+ this.token + "&customer_id=" + this.customer_id + "&").subscribe(res => {
                this.basket.getTimeBox().subscribe(resp1 => {
                  this.x = JSON.parse(JSON.stringify(res));
                  let y = JSON.parse(JSON.stringify(resp1));
                  console.log(y)
                  console.log(this.x);
                  this.storage.set('first_name', this.x.data.customer_info.fname)
                  this.storage.set('first_name', this.x.data.customer_info.fname);
                  this.storage.set('last_name', this.x.data.customer_info.lname);
                  this.storage.set('credit', this.x.data.customer_info.credit);
                  this.storage.set('mobile', this.x.data.customer_info.mobile);
                  this.storage.set('time_title', y.msg);
                  this.storage.set('addresses', JSON.stringify(this.x.data.addresses));
                });

              });

            }))
      // }
      // else {
      //   // this.rootPage = LoginPage;
      //   console.log(val);
      //
      // }
    // })
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }
  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
    if (key=='total'){
      this.n =value;
    }
    })
  }
  test() {

    this.loader.createLoader();
    this.loader.show();

    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      }
      if (key == 'token') {
        this.token = value;
      } //store these values as you wish

    }).then((resp) => {

      this.HomeService2.createLogin((this.loginparameter + this.token + "&customer_id=" + this.customer_id + "&"), response => this.navCtrl.push(this.bread, {
        param1: response, param2: this.loginparameter + this.token + "&customer_id=" + this.customer_id,
      }));
    });

    // this.HomeService2.createLogin((this.loginparameter+this.logintel+"&customer_id="+this.loginpass+"&"),response => this.navCtrl.push(this.home));

  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.loginparameter + this.token + "&customer_id=" + this.customer_id + "&"});
  }


}
