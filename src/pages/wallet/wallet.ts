import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';
import {FactorPage} from '../factor/factor';
import {Storage} from '@ionic/storage';
import {PayMetService} from '../paymet/paymet.service';
import {PardakhtPage} from '../pardakht/pardakht';

@Component({
  selector: 'page-home',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  factor=FactorPage;
  pardakht = PardakhtPage;
  n:any;
  param2:any;
  customer_id:any;
  token:any;
  credit:any=0;
  inp:any;
  x:any;
  constructor(private paymetservice:PayMetService, private storage: Storage,private alertCtrl: AlertController, private platform: Platform, public navParams: NavParams, public navCtrl: NavController, private viewCtrl: ViewController) {
    this.param2 = navParams.get('param2');
    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      }
      if (key == 'token') {
        this.token = value;
      }
      if (key == 'credit'){
        this.credit= value;
      }
    })

    }

  ionViewDidLoad() {

  }



  exitApp() {
    this.platform.exitApp();
  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: "token="+ this.token + "&customer_id=" + this.customer_id + "&"});
  }

  getcredi() {
    this.paymetservice.getcredit("addCreditAmount="+this.inp+"&token="+ this.token + "&customer_id=" + this.customer_id + "&").subscribe(
    res=>{
      this.x = JSON.parse(JSON.stringify(res));
      console.log(JSON.stringify(res));
      this.navCtrl.push(this.pardakht, {
        token: this.x.Token,
        url: this.x.url,
        type: this.x.type,
        redirect: this.x.Redirect
      }, error => {
        console.log(JSON.stringify(error));
      });
    }
    )
  }

  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
      if (key=='total'){
        this.n =value;
      }
    })
  }

}
