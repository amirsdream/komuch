import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AddrPage} from '../addr/addr';

let now = new Date();

@Component({
  selector: 'page-home',
  templateUrl: 'pay.html'
})
export class PayPage {
  buttonDisabled: any = [];
  param2: any;
  active: any = [];
  id: any = 0;
  addr = AddrPage;
  index: any[];

  x;

  constructor(public navParams: NavParams, public navCtrl: NavController) {
    this.param2 = navParams.get('param2');
    console.log(now.getHours());

    for (let i = 0; i < 8; i++) {
      this.buttonDisabled.push(true);
      this.active.push(false);
    }
    // if (now.getHours()>9){
    //   this.buttonDisabled[7] = true;
    // }
    if (now.getHours() > 9) {

      this.buttonDisabled[7] = false;
      this.buttonDisabled[6] = false;

    }
    // if (now.getHours()>18){
    //
    //   this.buttonDisabled[6] = true;
    //   this.buttonDisabled[7] = true;
    //   this.buttonDisabled[5] = true;
    // }
    if (now.getHours() > 18) {

      this.buttonDisabled[5] = false;
      this.buttonDisabled[6] = false;
      this.buttonDisabled[7] = false;
      this.buttonDisabled[4] = false;
    }

  }

  setActive(id) {
    this.active = [];
    for (let i = 0; i < 8; i++) {

      this.active.push(false);

    }
    ;
    this.active[id] = true;
    this.x = 1;
    console.log(this.active);

  }

  goToAddr() {
    if (this.x != 0) {
      this.navCtrl.push(this.addr, {
        param1: this.index, param2: this.param2

      });
    }
  }


}
