import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, MenuController, AlertController, Nav} from 'ionic-angular';
import {Network} from'@ionic-native/network';
import {LoginService2} from './login2.service';
import {NativeStorage} from '@ionic-native/native-storage';
import {HomePage} from '../home/home';
import {LoaderProvider} from './login2.loader';
import {baseUrl} from "../../base_url";
import {LoginPage} from '../login/login';
import { timer } from 'rxjs/observable/timer';
import {Observable} from 'rxjs/Observable';
import { take, map } from 'rxjs/operators';
import {setAppPackageJsonData} from "@ionic/app-scripts";

let loginparameter: string;
@Component({
  selector: 'page-home',
  templateUrl: 'login2.html'
})
export class LoginPage2 {
  @ViewChild(Nav) nav: Nav;

  home = HomePage;
  login1 = LoginPage;
  loginparameter = "token=1&flag=1&txtmobile=";
  logintel = "";
  loginpass = "";
  loginvalue = {'Posting params': this.loginparameter}
  setActive:boolean =false ;
  countDown;
  count = 120;

  constructor(private menu: MenuController,private alertCtrl: AlertController,private loader: LoaderProvider, private navParams: NavParams, private nativeStorage: NativeStorage, public navCtrl: NavController, private network: Network, public LoginService2: LoginService2) {
    this.logintel = navParams.get('logintel');
    this.countDown = Observable.timer(0, 1000).map(value => this.count - value).takeWhile(value => value > 0);
    this.countDown.subscribe(t=>{
      if (t==0){
        this.setActive=true
      }
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }


logForm() {
    this.loader.createLoader();
    this.loader.show();
    console.log(this.logintel);
    this.LoginService2.createLogin((this.loginparameter + this.logintel + "&txtpass=" + this.loginpass + "&"), response => {
      if(response){
      this.LoginService2.getPopupLogin().subscribe(resp => {
        this.loader.hide();
        let popup = JSON.parse(JSON.stringify(resp));
        console.log("------popup--");
        console.log(popup);
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: popup.popup,
          buttons: ['متوجه شدم']
        });
        alert.present();
        // this.navCtrl.push(this.home)
        this.navCtrl.setRoot(HomePage);
      });}
      else{
        this.loader.hide();
        this.setActive=true;
      }

    });
    // this.LoginService2.retrieveData(response => this.navCtrl.push(this.home));

  }

}

