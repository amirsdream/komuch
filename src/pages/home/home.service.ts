import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LoaderProvider } from '../login2/login2.loader';
import { Storage } from '@ionic/storage';
import {BreadService} from '../bread/bread.service';
import {baseUrl} from "../../base_url";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

// const params = new HttpParams()
//   .set('txtmobile', '09309129143')
//   .set('flag', '0')
//   .set('token','1');

// let params = new HttpParams();

export interface data1 {
  type: string;
  data: products;
}

export interface products {
  id: string;
  title: string;
  brief_description: string;
  description: string;
  status: string;
  sort_order: string;
  pic: string;
  price: string;
}




var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class HomeService2 {

  constructor(public bread : BreadService, private http:HttpClient,private loader: LoaderProvider,private storage: Storage) {}

  // Uses http.get() to load data from a single API endpoint
  x :any;
  isproducts : data1 ;
  products : products ;
  data: data1[];
  keys : any =[];
  y:any =0;


  createLogin(food,response) {

    let x: any;
    console.log(food);
    let body = JSON.stringify(food);
    this.http.post(baseUrl + 'front/frontcatalog/showProductPage',
      food, httpOptions)
      .subscribe( res  => {
        // this.loader.hide();
        x=JSON.parse(JSON.stringify(res));
        console.log(x);
        this.storage.set('cat_id', x.data.pcat_root[0].catـid);
        this.storage.set('cat_name', x.data.pcat_root[0].cat_name);

        console.log(x.data.pcat_root[0].cat_name);
        console.log("+++++++");
        if (x.type=="success"){

          this.x = this.bread.getbread(food, food).
          subscribe(res => {

            console.log(JSON.stringify(res));

            this.isproducts = res as data1;
            // console.log(JSON.stringify(this.isproducts));
            this.products = this.isproducts.data;
            this.keys=[];
            for (let key in this.products) {
              // keys.push({key: key, value: value[key]});
              for(let x in this.products[key]){
                this.products[key][x]['num']=0;
                this.keys.push({key:this.products[key][x],value:this.y});
                // console.log(value.key['id']);

              }
              // console.log(this.products);
            };
            response(this.keys);
            this.loader.hide();

          });
        };


        console.log(JSON.stringify(res));
      }, error => {
        this.loader.hide();
        console.log(JSON.stringify(error));
      });
    return x;

  }
  getTimeBox() {

    return this.http.post(baseUrl+'front/pages/getTimeBox',
      {},httpOptions)
      .map(res => res)


  }
}
