import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LoaderProvider } from '../login2/login2.loader';
import { Storage } from '@ionic/storage';
import {baseUrl} from "../../base_url";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

// const params = new HttpParams()
//   .set('txtmobile', '09309129143')
//   .set('flag', '0')
//   .set('token','1');

// let params = new HttpParams();

export interface data1 {
  type: string;
  data: products;
}

export interface products {
  id: string;
  title: string;
  brief_description: string;
  description: string;
  status: string;
  sort_order: string;
  pic: string;
  price: string;
  num: number ;
}



var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class BreadService {

  constructor(private http:HttpClient,private loader: LoaderProvider,private storage: Storage) {}

  // Uses http.get() to load data from a single API endpoint


  data: data1[];


  getbread(food,param2) {

    let x: any;
    console.log(food);
    let body = JSON.stringify(food);
    return this.http.post(baseUrl+'front/frontcatalog/showProducts',
      param2+"&cat_id="+"8"+"&", httpOptions)
      .map(res => res)

      // .subscribe( res  => {
      //   this.loader.hide();
      //   console.log(param2+"&cat_id="+"8"+"&");
      //   // console.log(JSON.stringify(res));
      //   // this.data = res as data1[];
      //   // response=this.data;
      //   // response = JSON.parse(JSON.stringify(res));
      //   // console.log(JSON.stringify(this.data));
      //
      // }, error => {
      //   this.loader.hide();
      //   console.log(JSON.stringify(error.json()));
      //   return 0;
      // });

  }
  async book(food,response) {

    let x: any;
    // console.log(param2+"&prductid="+food[0].key.id+"&qty="+food[0].value+"&vaherprice="+food[0].key.price+"&");

    console.log(food+"&");
    await this.http.post(baseUrl+'front/orderWithForm/bookingOrder',
      food+"&", httpOptions).toPromise();
      // .subscribe( res  => {
      //
      //   x=JSON.parse(JSON.stringify(res));
      //   console.log(x.type);
      //
      //   response(res);
      //   x=res;
      //   console.log(JSON.stringify(res))
      //   ;
      //
      // }, error => {
      //   // this.loader.hide();
      //   console.log(JSON.stringify(error));
      // });
    return x;

  }
}
