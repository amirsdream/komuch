import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';
import {InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser';
import {Storage} from '@ionic/storage';
import {baseUrl} from '../../base_url';
import {HomePage} from '../home/home';
@Component({
  selector: 'page-home',
  templateUrl: 'map.html',
})

export class MapPage {

  constructor(private storage: Storage,private iab: InAppBrowser, private alertCtrl: AlertController, private platform: Platform, public navParams: NavParams, public navCtrl: NavController, private viewCtrl: ViewController) {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      location: 'no',
      hardwareback: 'no',
    }

    const browser = this.iab.create(baseUrl+'front/pages/map', '_blank', options);
    browser.show();





  }
  retHome(){
    this.navCtrl.setRoot(HomePage);
  }

}
