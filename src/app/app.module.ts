import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {Network} from '@ionic-native/network';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {LoginPage2} from '../pages/login2/login2';
import {FactorPage} from '../pages/factor/factor';
import {BreadPage} from '../pages/bread/bread';
import {DiscountPage} from '../pages/discount/discount';
import {ListPage} from '../pages/list/list';
import {HttpClientModule} from '@angular/common/http';
import {NavController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginService} from '../pages/login/login.service';
// import {NextPage} from '../pages/login2/login2.next';
import {GoogleMaps}from '@ionic-native/google-maps';
import {LoginService2} from '../pages/login2/login2.service';
import {BreadService} from '../pages/bread/bread.service';
import {HomeService2} from '../pages/home/home.service';
import {FactorService} from '../pages/factor/factor.service';
import {NativeStorage} from '@ionic-native/native-storage';
import {IonicStorageModule} from '@ionic/storage';
import {LoadingController} from 'ionic-angular';
import {LoaderProvider} from'../pages/login2/login2.loader';
import {CustomPipe} from '../pages/bread/pipe';
// import {MapPage} from '../pages/gmaps/gmaps';
import {TimePage} from '../pages/time/time';
import {PayPage} from '../pages/pay/pay';
import {AddrPage} from '../pages/addr/addr';
import {AddrService} from '../pages/addr/addr.service';
import {Geolocation} from '@ionic-native/geolocation';
import {NativeGeocoder} from '@ionic-native/native-geocoder';
import {NointernetPage} from '../pages/nointernet/nointernet';
import {TimeService} from '../pages/time/time.service';
import {PayMetPage} from '../pages/paymet/paymet';
import {PayMetService} from '../pages/paymet/paymet.service';
import {WalletPage} from '../pages/wallet/wallet';
import {DateService} from '../pages/date/date.service';
import {PardakhtPage} from '../pages/pardakht/pardakht';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BasketService } from './app.service';
import {ProfileService} from '../pages/profile/profile.service';
import {HTTP} from '@ionic-native/http';
import {DatePage} from "../pages/date/date";
import {MapPage} from "../pages/map/map";
import {AboutPage} from "../pages/about/about"
import {ProfilePage} from '../pages/profile/profile';
// import {PersianPipesModule} from 'angular2-persian-pipes'
import {CustomPipeNumber} from './pipenumber';
import {NetworkProvider} from "./networkprovider";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LoginPage2,
    ListPage,
    FactorPage,
    BreadPage,
    CustomPipe,
    CustomPipeNumber,
    MapPage,
    TimePage,
    AddrPage,
    NointernetPage,
    PayMetPage,
    WalletPage,
    PardakhtPage,
    PayPage,
    DiscountPage,
    DatePage,
    MapPage,
    AboutPage,
    ProfilePage,
  ],
  imports: [
    HttpClientModule,

    BrowserModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement: 'top',
      backButtonText: 'بازگشت'
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LoginPage2,
    ListPage,
    BreadPage,
    FactorPage,
    MapPage,
    TimePage,
    AddrPage,
    NointernetPage,
    PayMetPage,
    WalletPage,
    PardakhtPage,
    PayPage,
    DiscountPage,
    DatePage,
    MapPage,
    AboutPage,
    ProfilePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    LoginService,
    LoginService2,
    FactorService,
    NativeStorage,
    IonicStorageModule,
    LoadingController,
    LoaderProvider,
    HomeService2,
    BreadService,
    GoogleMaps,
    AddrService,
    Geolocation,
    NativeGeocoder,
    TimeService,
    PayMetService,
    InAppBrowser,
    BasketService,
    DateService,
    ProfileService,
    HTTP,
    NetworkProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
