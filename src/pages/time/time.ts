import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AddrPage} from '../addr/addr';
import {LoaderProvider} from '../login2/login2.loader';
import {TimeService} from './time.service';
import {Storage} from '@ionic/storage';
import {FactorPage} from "../factor/factor";
import {AlertController} from 'ionic-angular';
import { ToastController } from 'ionic-angular';

let now = new Date();

@Component({
  selector: 'page-home',
  templateUrl: 'time.html'
})
export class TimePage {
  buttonDisabled: any = [];
  param2: any;
  active: any = [];
  public now: Date = new Date();
  id: any = 0;
  addr = AddrPage;
  index: any[];
  param1: any;
  msg_param: any;
  minute:any;
  hour:any;
  n;
  gap:any;
  x: any;
  y: any = 0;
  factor = FactorPage;
  factors: any;
  selected_time = 0;
  btn_title = 'انتخاب زمان تحویل';
  msg_time: any;
  msg_title = '';

  constructor(private toastCtrl: ToastController,private alertCtrl: AlertController, private storage: Storage, private time: TimeService, private loader: LoaderProvider, public navParams: NavParams, public navCtrl: NavController) {
    this.param2 = navParams.get('param2');
    this.param1 = navParams.get('param1');
    this.factors = navParams.get('param2');
    this.now = new Date();
    this.hour=this.now.getHours();
    this.minute=this.now.getMinutes();
    console.log(this.now.getMinutes());
    console.log(this.now.getHours());
    this.gap=this.factors.gap_time;

    this.factors.days.today_title = this.btn_title;
    this.factors.days.today_active = false;
    this.factors.days.tomorrow_title = this.btn_title;
    this.factors.days.tomorrow_active = false;

    console.log(this.param1);
    console.log(this.factors);

    this.storage.forEach((value, key) => {
      if (key == "time_title") {
        this.msg_title = value;
      }
    });

    // console.log(now.getHours());
    //
    // for (let i = 0; i < 8; i++) {
    //   this.buttonDisabled.push(true);
    //   this.active.push(false);
    // }
    // if (now.getHours() > 7) {
    //   this.buttonDisabled[7] = false;
    // }
    // if (now.getHours() > 9) {
    //
    //   this.buttonDisabled[7] = false;
    //   this.buttonDisabled[6] = false;
    //
    // }
    // if (now.getHours() > 17) {
    //
    //   this.buttonDisabled[6] = true;
    //   this.buttonDisabled[7] = true;
    //   this.buttonDisabled[5] = true;
    // }
    // if (now.getHours() > 19) {
    //
    //   this.buttonDisabled[5] = false;
    //   this.buttonDisabled[6] = false;
    //   this.buttonDisabled[7] = false;
    //   this.buttonDisabled[4] = false;
    // }

  }

  setActive(id) {
    console.log(id)
    let alert_title = '';
    let inputs = [];
    if (!id) {
      // this.factors.days.today_active = true;
      // this.factors.days.tomorrow_active = false;
      alert_title = "امروز - " + this.factors.days.today;

      this.factors.time_interval.forEach((time) => {
        let input = new Input();
        if ((time.start*60-this.gap)>((this.hour*60)+(this.minute))){
        input.value = time.time_id;
        input.label = time.text;
        console.log(time);
        input.type = "radio";
        if (time.status) {
          input.disabled = false;
        } else {
          input.disabled = true;
        }
        inputs.push(input);
        }
      })

    } else {


      // this.factors.days.today_active = false;
      // this.factors.days.tomorrow_active = true;

      alert_title = "فردا - " + this.factors.days.tomorrow;

      this.factors.time_interval_tomorrow.forEach((time) => {

        let input = new Input();

        input.value = time.time_id;
        input.label = time.text;
        input.type = "radio";

        if (time.status) {
          input.disabled = false;
        } else {
          input.disabled = true;
        }

        inputs.push(input);
      })

    }


    let alert = this.alertCtrl.create({
      title: '<span>' + alert_title + '</span><img style="margin-left: 10px;" width="20" height="20" src="assets/imgs/icon.png"></img>',
      inputs: inputs,

      buttons: [
        {
          text: 'انصراف',
          role: 'cancel',
          handler: () => {
            console.log(this.selected_time)
            console.log(this.factors.days.today_active);
            console.log(this.factors.days.tomorrow_active);
          }
        },
        {
          text: 'تایید',
          handler: (data) => {
            if (data == undefined) {
              this.selected_time = 0;
              this.factors.days.tomorrow_title = this.btn_title;
              this.factors.days.today_title = this.btn_title;
            } else {

              this.selected_time = data;

              if (id) {
                this.factors.days.today_active = false;
                this.factors.days.tomorrow_active = true;
                this.factors.days.today_title = this.btn_title;
                this.factors.days.tomorrow_title = this.factors.time_interval_tomorrow.filter(time => time.time_id == data)[0].text;
                this.storage.set('date_text',this.factors.days.tomorrow_title);
              } else {
                this.factors.days.today_active = true;
                this.factors.days.tomorrow_active = false;
                this.factors.days.tomorrow_title = this.btn_title;
                this.factors.days.today_title = this.factors.time_interval.filter(time => time.time_id == data)[0].text;
                this.storage.set('date_text', this.factors.days.today_title);
              }

            }
            console.log(data)
            // I NEED TO GET THE VALUE OF THE SELECTED RADIO BUTTON HERE
          }
        }
      ]
    });

    alert.present();

  }


  goToAddr() {


    if (this.selected_time != 0) {

      if (this.factors.days.today_active) {

        this.index = ["today", this.selected_time];

      } else {

        this.index = ["tomorrow", this.selected_time];

      }

      this.loader.createLoader();
      this.loader.show();
      this.time.getprofile(this.param1).subscribe(res => {
        this.time.getPaymentBox().subscribe(resp => {
          this.time.getTimeBox().subscribe(resp1 => {
            console.log(resp);
            console.log(resp1);
            this.msg_param = JSON.parse(JSON.stringify(resp));
            this.msg_time = JSON.parse(JSON.stringify(resp1));
            this.msg_title = this.msg_time.msg;
            console.log(this.msg_time)
            console.log(this.msg_title)
            this.loader.hide();
            this.x = JSON.parse(JSON.stringify(res));
            console.log(this.x);
            this.storage.set('first_name', this.x.data.customer_info.fname);
            this.storage.set('last_name', this.x.data.customer_info.lname);
            this.storage.set('credit', this.x.data.customer_info.credit);
            this.storage.set('mobile', this.x.data.customer_info.mobile);
            this.storage.set('addresses', JSON.stringify(this.x.data.addresses));
            console.log("++++++");
            console.log(this.param1);
            console.log("++++++");
            this.storage.set('timeinfo', JSON.stringify(this.index));
            this.navCtrl.push(this.addr, {
              param1: this.index, param2: this.param2, param3: this.param1, msg_param: this.msg_param,
            });
          }, error => {
            this.loader.hide();
            console.log(JSON.stringify(error));
          });

        }, error => {
          this.loader.hide();
          console.log(JSON.stringify(error));
        });

      }, error => {
        this.loader.hide();
        console.log(JSON.stringify(error));
      });
    }
    if(this.selected_time == 0){
      let toast = this.toastCtrl.create({
        message: 'لطفا زمان تحویلو انتخاب کنید',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.param1});
  }

  ionViewDidEnter() {
    this.storage.forEach((value, key) => {
      if (key == 'total') {
        this.n = value;
      }
    })
  }
}

class Button {
  title: string = '';
  active: boolean = false;
}
class Input {
  type: string;
  value: string;
  label: string;
  disabled: boolean;
  name: string;
}

