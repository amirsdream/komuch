import {Component} from '@angular/core';
import {NavController, ViewController, NavParams} from 'ionic-angular';
import {BreadPage} from '../bread/bread';
import {LoaderProvider} from '../login2/login2.loader';
import {AddrService} from './addr.service';
import {Storage} from '@ionic/storage';
// import {MapPage} from'../gmaps/gmaps';
import {FactorPage} from '../factor/factor';
// import {NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult} from '@ionic-native/native-geocoder';
import {PayMetPage} from '../paymet/paymet';
// import {LatLng} from "@ionic-native/google-maps";
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'addr.html',
})
export class AddrPage {
  gcode = "خودتو تو نقشه پیدا کن!";
  customer_id = "";
  token = "";
  first_name = " ";
  last_name = " ";
  // map = MapPage;
  param1: any;
  param2: any;
  param3: any;
  param5: any;
  param4: any;
  factor=FactorPage;
  n;
  lat: any="خالی";
  lng: any="خالی";
  addr: any;
  newaddr: any = [];
  // private location: LatLng;
  keys: any = [];
  input;
  last;
  paymet = PayMetPage;
  msg_param:any;
  msg_time:any;

  constructor(private toastCtrl: ToastController, public navParams: NavParams, private storage: Storage, public AddrService: AddrService, private loader: LoaderProvider, public navCtrl: NavController, private viewCtrl: ViewController) {
    this.param2 = navParams.get('param2');
    this.param1 = navParams.get('param1');
    this.param3 = navParams.get('param3');
    this.msg_param = navParams.get('msg_param');
    this.msg_time = navParams.get('msg_param');
    // this.location = navParams.get('location');

    // if (this.location != null) {
    //   this.lat = this.location.lat;
    //   this.lng = this.location.lng;
    //   this.nativeGeocoder.reverseGeocode(this.location.lat, this.location.lng)
    //     .then((result: NativeGeocoderReverseResult) => this.gcode = JSON.stringify(result[0].countryName + ", " + result[0].locality + ", " + result[0].thoroughfare))
    //     .catch((error: any) => console.log(error));
    // }

    console.log(this.param3);


    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      }
      if (key == 'token') {
        this.token = value;
      } //store these values as you wish

      if (key == 'first_name') {
        if (value!=null) {
          this.first_name = value;
        }
      } //store these values as you wish

      if (key == 'last_name') {
        if (value!=null) {
          this.last_name = value;
        }
      } //store these values as you wish

      if (key == 'addresses') {
        this.addr = JSON.parse(value);
        console.log(value)
      }
    }).then(resp => this.getData())


  }

  getData() {
    for (let key in this.addr) {
      console.log(key);
      this.keys.push({key: this.addr[key]['address_text'], value: this.addr[key]['id']});
      console.log(this.keys);
    }
    return this.addr
  }

  // goToMap() {
  //   this.navCtrl.push(this.map, {param1: this.param1, param2: this.param2, param3: this.param3});
  // }

  select(item) {
    this.param4 = item;
    console.log(this.param4)
  }


  goToLast() {
    this.storage.set('first_name', this.first_name);
    this.storage.set('last_name', this.last_name);
    if (this.param4 != null) {
      this.lat = "خالی";
      this.lng = "خالی";
      this.param5 = [this.lat, this.lng]
      this.navCtrl.push(this.paymet, {
        param1: this.param1, param2: this.param2, param3: this.param3, param4: this.param4,param5 :this.param5,msg_param:this.msg_param

      });

    } else {
      if (this.input) {
        console.log(this.param2);
        this.newaddr = {key: "", value: ""};
        this.newaddr.key = this.input;
        this.newaddr.value = "new";
        this.param4 = this.newaddr;
        this.param5 = [this.lat, this.lng];
        this.navCtrl.push(this.paymet, {
          param1: this.param1, param2: this.param2, param3: this.param3, param4: this.param4,param5 :this.param5, msg_time: this.msg_time

        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'لطفا همه فیلد هارو پر کنید',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    }
  }
  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.param3});
  }

  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
      if (key=='total'){
        this.n =value;
      }
    })
  }

}
