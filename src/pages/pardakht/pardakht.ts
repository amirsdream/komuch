import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';
import {InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'pardakht.html',
})

export class PardakhtPage {
  param1: any;
  param2: any;
  param3: any;
  param4: any;
  script: any;
  text1: any = 'function postRefId (refIdValue) {var form = document.createElement("form");form.setAttribute("method", "POST");form.setAttribute("action", "'
  // https://sep.shaparak.ir/payment.aspx
  text2: any =  '");form.setAttribute("target", "_self");var hiddenField = document.createElement("input");hiddenField.setAttribute("name", "Token");hiddenField.setAttribute("value", refIdValue);form.appendChild(hiddenField);var hiddenField1 = document.createElement("input");hiddenField1.setAttribute("name", "RedirectURL");hiddenField1.setAttribute("value","'
  // http://komuch.ir/front/userOrder/VerifyPayment/saman
  text3:any ='");form.appendChild(hiddenField1);document.body.appendChild(form);form.submit();document.body.removeChild(form);};postRefId("'
  text4: any = '");';
  token: any;

  constructor(private storage: Storage,private iab: InAppBrowser, private alertCtrl: AlertController, private platform: Platform, public navParams: NavParams, public navCtrl: NavController, private viewCtrl: ViewController) {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      location: 'no',
      hardwareback: 'no',

    }
    this.param1 = navParams.get('token');
    // this.token = this.param1;
    console.log(this.param1)
    this.param2 = navParams.get('url');
    this.param3 = navParams.get('redirect');
    this.param4 = navParams.get('type');
    const browser = this.iab.create('about:blank','_blank',);
    browser.show();
    // browser.executeScript({
    //   code: this.text1 + this.param2 + this.text2+this.param3+this.text3+this.param1+this.text4
    // });
      // .then((e) => {
      // browser.on('loadstop').subscribe(event => {
      //   if (event.url.includes(this.param3)) {
      //     //payment gateway call sucess.
      //     browser.close();
      //     this.storage.set('total',0);
      //     this.storage.set('finalprice',0);
      //     this.storage.set('timeinfo',0);
      //     this.navCtrl.popToRoot();
      //   }
      //
      // })

    const subs = browser.on('loadstop').subscribe(event => {
      browser.executeScript({
        code: this.text1 + this.param2 + this.text2+this.param3+this.text3+this.param1+this.text4
      }).then((e) => {
        subs.unsubscribe();
        const sub2 = browser.on('loadstop').subscribe(event => {
          // this.iab.create(event.url,'_system');
          // sub2.unsubscribe();
          if (event.url.includes(this.param3)) {
            //payment gateway call sucess.
            browser.close();
            this.storage.set('total',0);
            this.storage.set('finalprice',0);
            this.storage.set('timeinfo',0);
            this.navCtrl.popToRoot();
          }

        })
      }).catch((e) => {
        console.log('Erro ao adicionar JS. ' + e);
      });


    });


  }
  retHome(){
    this.navCtrl.popToRoot();
  }

}
