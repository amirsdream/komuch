import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LoaderProvider } from '../login2/login2.loader';
import { Storage } from '@ionic/storage';
import {baseUrl} from "../../base_url";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

// const params = new HttpParams()
//   .set('txtmobile', '09309129143')
//   .set('flag', '0')
//   .set('token','1');

// let params = new HttpParams();





var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class PayMetService {

  constructor(private http:HttpClient,private loader: LoaderProvider,private storage: Storage) {}

  // Uses http.get() to load data from a single API endpoint





  getdiscount(food) {

      let x: any;
      console.log(food);
      let body = JSON.stringify(food);
      return this.http.post(baseUrl+'front/userOrder/checkoutOffCode',
        food, httpOptions)
        .map(res => res)


  }

  getcredit(food) {

    let x: any;
    console.log(food);
    let body = JSON.stringify(food);
    return this.http.post(baseUrl+'front/useraccounts/addCredit',
      food, httpOptions)
      .map(res => res)


  }

  goTo(food) {

      let x: any;
      console.log(food);
      let body = JSON.stringify(food);
      return this.http.post(baseUrl+'front/userOrder/checkoutOrder',
        food, httpOptions)
        .map(res => res)


  }
  deletefactor(food) {

    let x: any;
    console.log(food);
    let body = JSON.stringify(food);
    return this.http.post(baseUrl+'front/userOrder/DeletePO',
      food, httpOptions)
      .map(res => res)


  }
}
