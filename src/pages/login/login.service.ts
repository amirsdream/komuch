import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams,HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { LoaderProvider } from '../login2/login2.loader';
import {HTTP} from '@ionic-native/http';
import {baseUrl} from "../../base_url";
import { ToastController } from 'ionic-angular';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/x-www-form-urlencoded',
  })
};

const params = new HttpParams()
  .set('txtmobile', '09309129143')
  .set('flag', '0')
  .set('token','1');

// let params = new HttpParams();
var body = 'flag=0&token=1&txtmobile=';
var headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable()
export class LoginService {
  y:any;

  constructor(private toastCtrl: ToastController,private http:HttpClient,private loader: LoaderProvider) {
    console.log(baseUrl)
  }

  // Uses http.get() to load data from a single API endpoint





  createLogin(food,response) {
    console.log(baseUrl)
    let x:string;
    console.log(food);
    let body = JSON.stringify(food);
    console.log(params);
    this.http.post(baseUrl+'front/useraccounts/login',
      body, httpOptions)
      .subscribe(res => {
        this.loader.hide();
        response(res)
        this.y = JSON.parse(JSON.stringify(res));

        console.log(res);
        console.log(JSON.stringify(res));
        let toast = this.toastCtrl.create({
          message: this.y.msg,
          duration: 3000,
          position: 'top',
        });

        toast.present();
      }, error => {
        this.loader.hide();
        console.log(JSON.stringify(error));
      });
  }
}
