import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { LoaderProvider } from '../login2/login2.loader';
import {DateService} from './date.service';
import {Storage} from '@ionic/storage';
import {FactorPage} from "../factor/factor";


/**
 * Generated class for the DatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-date',
  templateUrl: 'date.html',
})
export class DatePage {

  len = [1,2,2,2,2,2];
  customer_id:any;
  token:any;
  orders:any = [];
  factor = FactorPage;
  loginparameter = "token=";
  n:any;

  times = [
    {month:1,description:"فروردین"},
    {month:2,description:"اردیبهشت"},
    {month:3,description:"خرداد"},
    {month:4,description:"تیر"},
    {month:5,description:"مرداد"},
    {month:6,description:"شهریور"},
    {month:7,description:"مهر"},
    {month:8,description:"آبان"},
    {month:9,description:"آذر"},
    {month:10,description:"دی"},
    {month:11,description:"بهمن"},
    {month:12,description:"اسفند"},
  ]

  constructor(public navCtrl: NavController,private storage: Storage,private dateService:DateService,private loader: LoaderProvider, public navParams: NavParams) {

    this.storage.forEach((value, key) => {
      if (key == 'customer_id') {
        this.customer_id = value;
        // console.log(customer_id);
      } if (key == 'token') {
        this.token = value;
      } }).then(()=>{

        this.loader.createLoader();
      this.loader.show();
      this.dateService.getHistory("token="+ this.token + "&customer_id=" + this.customer_id + "&").subscribe(res => {

        console.log("histoory-------------");
        console.log(res);
        this.orders = JSON.parse(JSON.stringify(res)).data.orders;
        console.log(this.orders);

        this.orders.forEach(order => {

          var time = order.date_tahvil.split('/');

          let search_time = this.times.filter(t => t.month == time[1] );

          if(search_time.length > 0 ){
            order.desc_time = search_time[0].description;
            order.day = time[2];
            // console.log(order.desc_time);
          }


        });
        this.loader.hide();

      },error => {
        this.loader.hide();
      })

    })



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DatePage');
  }
  ionViewDidEnter(){
    this.storage.forEach((value, key) => {
      if (key=='total'){
        this.n =value;
      }
    })
  }

  getfactor() {
    this.navCtrl.push(this.factor, {param2: this.loginparameter + this.token + "&customer_id=" + this.customer_id + "&"});
  }

}
