import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import {PayMetService} from "../paymet/paymet.service";
import {LoaderProvider} from '../login2/login2.loader';
import {BasketService} from "../../app/app.service";
import {PayMetPage} from "../paymet/paymet";
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the DiscountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-discount',
  templateUrl: 'discount.html',
})
export class DiscountPage {

  param3: any;
  param2: any;
  param1: any;
  param4: any;
  param5: any;
  input:any="";
  discount_success_data:any;
  success_discount_code=false;
  full_price = 0;
  paymetPage=PayMetPage;
  msg_param:any;

  constructor(private toastCtrl: ToastController,public navCtrl: NavController,private basket: BasketService, private loader: LoaderProvider,private pay: PayMetService, public navParams: NavParams,private alertCtrl: AlertController,) {
    this.param3 = navParams.get('param3');
    this.param2 = navParams.get('param2');
    this.param4 = navParams.get('param4');
    this.param1 = navParams.get('param1');
    this.param5 = navParams.get('param5');
    this.msg_param = navParams.get('param5');
    // console.log(this.param3);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiscountPage');
  }

  sendCode(){
    let x: any;
    // this.loader.createLoader();
    // this.loader.show();
    if(this.input.length > 0) {
      let x: any;
      this.loader.createLoader();
      this.loader.show();
      this.pay.getdiscount(this.param3 + "&txtoff=" + this.input + "&").subscribe(res => {
        this.loader.hide();
        console.log(JSON.stringify(res));
        x = JSON.parse(JSON.stringify(res));
        if (x.type == 'error') {
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: x.msg,
            buttons: ['متوجه شدم']
          });
          alert.present();
        } else {
          this.basket.getbasket(this.param3+ "&")
            .subscribe(res => {
              this.discount_success_data = res;
              console.log( this.discount_success_data);
              this.success_discount_code = true;
              this.full_price = this.discount_success_data.data.orders[0].final_price;
              console.log(JSON.stringify(res));
              console.log(this.discount_success_data.data.orders[0].final_price)
            });
        }
      }, error => {
        this.loader.hide();
        console.log(JSON.stringify(error));
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'کد تخفیفو وارد کن',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

  }
  verify(){
    this.navCtrl.push(this.paymetPage, {param1:this.param1,param2:this.param2,param3: this.param3,param4:this.param4,param5:this.param5,param6:this.discount_success_data});

  }
}
