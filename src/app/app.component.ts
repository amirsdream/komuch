import {Component, ViewChild} from '@angular/core';
import {AlertController, Nav, Platform, Events} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Network} from '@ionic-native/network';
import {HomePage} from '../pages/home/home';
// import { HomePage2 } from '../pages/home/home2';
import {LoginPage} from '../pages/login/login';
import {ListPage} from '../pages/list/list';
import {WalletPage} from '../pages/wallet/wallet';
import {BreadPage} from '../pages/bread/bread';
import {Storage} from '@ionic/storage';
import {NointernetPage} from '../pages/nointernet/nointernet';
import {BasketService} from './app.service';
import {TimeService} from'../pages/time/time.service';
import {DatePage} from "../pages/date/date";
import {MapPage} from "../pages/map/map";
import {AboutPage} from "../pages/about/about";
import {ProfilePage} from "../pages/profile/profile";
import {NetworkProvider} from "./networkprovider";

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  mobile: any = "";
  rootPage: any;
  firstname: any = "";
  lastname: any = "";
  customer_id:any ="";
  token:any ="";
  x:any;

  isOffline(): boolean {
    return navigator.onLine;
  }

  pages: Array<{ title: string, component: any }>;

  constructor(public events: Events, public network: Network, public networkProvider: NetworkProvider,private alertCtrl: AlertController,private time: TimeService,private basket: BasketService, private storage: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {


    let res: boolean;

    if (!this.isOffline()) {
      this.rootPage = NointernetPage;
    }
    else {
      this.testInput();

    }

    this.storage.forEach((value, key) => {
      if (key == 'first_name') {
        this.firstname = value;
        // console.log(customer_id);
      }
      if (key == 'last_name') {
        this.lastname = value;
      } //store these values as you wish

      if (key == 'mobile') {
        this.mobile = value;
      } //store these values as you wish

    })
    this.initializeApp();


    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'خانه', component: HomePage},
      // { title: 'Home2', component: HomePage },
      {title: 'وضیعت سفارش', component: ListPage},
      {title: 'کیف پول', component: WalletPage},
      {title: 'exit', component: LoginPage},
      {title: 'تاریخجه', component: DatePage},
      {title: 'مناطق تحت پوشش', component: MapPage},
      {title: ' تماس با ما', component: AboutPage},
      {title:'پزوفایل',component:ProfilePage}

    ];


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.networkProvider.initializeNetworkEvents();
      // this.testInput();
      // Offline event
      // this.events.subscribe('network:offline', () => {
      //   this.rootPage = NointernetPage;
      // });
      //
      // // Online event
      // this.events.subscribe('network:online', () => {
      //   this.testInput();
      // });
      // let disconnectSub = Network.onDisconnect().subscribe(() => {
      //   this.rootPage = NointernetPage;
      // });
      //
      // let connectSub = Network.onConnect().subscribe(()=> {
      //   this.testInput();
      // });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  testInput(){

    this.storage.get('loggedin').then((val) => {
      if (val == "success") {
        this.rootPage = HomePage;
        console.log(1);
        this.storage.forEach((value, key) => {
          if (key == 'customer_id') {
            this.customer_id = value;
            // console.log(customer_id);
          } if (key == 'token') {
            this.token = value;
          } })
          .then(resp => this.basket.getbasket("token="+ this.token + "&customer_id=" + this.customer_id + "&")
            .subscribe(res => {
              console.log(JSON.stringify(res));
              this.time.getprofile("token="+ this.token + "&customer_id=" + this.customer_id + "&").subscribe(res => {
                this.basket.getTimeBox().subscribe(resp1 => {
                  this.x = JSON.parse(JSON.stringify(res));
                  let y = JSON.parse(JSON.stringify(resp1));
                  console.log(y)
                  console.log(this.x);
                  this.storage.set('first_name', this.x.data.customer_info.fname)
                  this.storage.set('first_name', this.x.data.customer_info.fname);
                  this.storage.set('last_name', this.x.data.customer_info.lname);
                  this.storage.set('credit', this.x.data.customer_info.credit);
                  this.storage.set('mobile', this.x.data.customer_info.mobile);
                  this.storage.set('time_title', y.msg);
                  this.storage.set('addresses', JSON.stringify(this.x.data.addresses));
                });

              });

            }))
      }
      else {
        this.rootPage = LoginPage;
        console.log(val);

      }
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  signOut() {
    let alert = this.alertCtrl.create({
      title: 'توجه',
      message: 'از این حسابت خارج میشی؟',

      buttons: [
        {
          text: 'انصراف',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'تایید',
          handler: (data) => {
            this.storage.set('loggedin', "").then(() => {
              this.nav.setRoot(LoginPage);
            })
          }


        }
      ]
    });
    alert.present();
  }

}
