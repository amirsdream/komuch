import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { Loading,LoadingController } from 'ionic-angular';


@Injectable()
export class LoaderProvider {
  loading: Loading;

  constructor( public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.createLoader();
  }

  createLoader(message: string = "") { // Optional Parameter
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  show() {
    this.loading.present();
  }

  hide() {
    this.loading.dismiss().catch();
    // this.loading.dismiss();
  }


}
