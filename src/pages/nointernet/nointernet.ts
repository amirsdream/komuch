import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, Platform, AlertController} from 'ionic-angular';
import {MyApp} from '../../app/app.component';

@Component({
  selector: 'page-home',
  templateUrl: 'nointernet.html',
})
export class NointernetPage {


  constructor(private alertCtrl: AlertController, private platform: Platform, public navParams: NavParams, public navCtrl: NavController, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.presentAlert()
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'قطع اینترنت',
      subTitle: ' اینترنت قطع می باشد',
      buttons: ['متوجه شدم']
    });
    alert.present();
  }


  exitApp() {
    this.platform.exitApp();
  }
}
